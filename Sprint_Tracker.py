import requests
import configparser
config = configparser.ConfigParser()
from datetime import date, timedelta
config.read('JIRA_CONFIG.ini')
SERVER=(config['JIRA']['SERVER'])
USER=(config['JIRA']['USER'])
PASSWORD=(config['JIRA']['PASSWORD'])
proxyDict = { 
              "http"  : (config['JIRA']['HTTP_PROXY']), 
              "https" : (config['JIRA']['HTTPS_PROXY']) 
              
            }
def get_scrums():
    scrums=[]
    r=requests.get(SERVER+"/rest/agile/1.0/board/",auth=(USER,PASSWORD), proxies=proxyDict)

    ALL_BOARDS=(r.json()['values'])
    for a in ALL_BOARDS:
        if(a["type"]=="scrum"):
        
            projectName=a["location"]["projectName"]
            id=a["id"]
            projectKey=a["location"]["projectKey"]
            d={"ID":id,"KEY":projectKey,"NAME":projectName}
            scrums.append(d)
    return scrums
def get_sprints(id):
    sprints=[]
    r=requests.get(SERVER+"/rest/agile/1.0/board/"+str(id)+"/sprint",auth=(USER,PASSWORD), proxies=proxyDict)
    ALL_SPRINTS=(r.json()["values"])
    for a in ALL_SPRINTS:
        
            sprint_id=a['id']
            #start_date=a['startDate'].split("T")[0]
            #end_date=a['endDate'].split("T")[0]
            sprint_name=a['name']
            d={"sprint_id":sprint_id,"sprint_name":sprint_name,"state":a['state']}
            sprints.append(d)
    return sprints
def get_backlogs(id):
    BRES=[]
    jiraRes=requests.get(SERVER+"/rest/agile/1.0/board/"+str(id)+"/backlog",auth=(USER,PASSWORD), proxies=proxyDict)
    jiraResJson=jiraRes.json()
    back_tot=(jiraResJson['total'])
    if not back_tot==0:
        back_iss=(jiraResJson['issues'])
    
        for k in back_iss:
            BKEY=(k['key'])
            BSUM=(k['fields']['summary'])
            BDES=(k['fields']['description'])
            BDIC={"BKEY":BKEY,"BSUM":BSUM,"BDES":BDES}
            BRES.append(BDIC)
    if back_tot==0:
        BKEY="No Backlog"
        BSUM="No Backlog"
        BDES="No Backlog"
        BDIC={"BKEY":BKEY,"BSUM":BSUM,"BDES":BDES}
        BRES.append(BDIC)
    return BRES,back_tot
def get_all(PROJKEY):
    defects=[]
    stories=[]
    all_issues=[]
    dates=[]
    jiraRes=requests.get(SERVER+"/rest/api/2/search?jql=project="+PROJKEY+"&maxResults=1000",auth=(USER,PASSWORD), proxies=proxyDict)
    jiraResJson=jiraRes.json()
    total=jiraResJson["total"]
    issues=jiraResJson["issues"]
    for i in issues:
        if(i["fields"]["issuetype"]["name"]=="Defect" or i["fields"]["issuetype"]["name"]=="Defects" or i["fields"]["issuetype"]["name"]=="defect"):
            d={"issue_key":i["key"],"issue_summary":i["fields"]["summary"],"issue_description":i["fields"]["description"],"issue_status":i["fields"]["status"]["name"],"created":i["fields"]["created"].split("T")[0]}
            defects.append(d)
            
            
        if(i["fields"]["issuetype"]["name"]=="Story" or i["fields"]["issuetype"]["name"]=="Stories" or i["fields"]["issuetype"]["name"]=="story"or i["fields"]["issuetype"]["name"]=="Userstory"):
            d={"issue_key":i["key"],"issue_summary":i["fields"]["summary"],"issue_description":i["fields"]["description"],"issue_status":i["fields"]["status"]["name"],"created":i["fields"]["created"].split("T")[0]}
            stories.append(d)
            
            
        else:
            pass
    print(defects)
    print(stories)
def get_issuesByDate(PROJKEY,sprint_id):
    calender_defect=[]
    calender_story=[]
    calender_backlogs=[]
    if sprint_id=="ALL":
        for d in range(0,7):
            dt=date.today() - timedelta(d)
            URL=SERVER+"/rest/api/2/search?jql=created%20%3E=%20%22"+str(dt)+"%22%20and%20created%20%3C=%20%22"+str(dt+timedelta(1))+"%22and%20project="+PROJKEY+" and%20issuetype=Defect"
            print(URL)
            jiraRes=requests.get(URL,auth=(USER,PASSWORD), proxies=proxyDict)
            no=jiraRes.json()["total"]
        
        
       
            d={"Date":str(dt).split("-")[1]+"/"+str(dt).split("-")[2],"NO":no}
            calender_defect.insert(0,d)
        for d in range(0,7):
            dt=date.today() - timedelta(d)
            URL=SERVER+"/rest/api/2/search?jql=created%20%3E=%20%22"+str(dt)+"%22%20and%20created%20%3C=%20%22"+str(dt+timedelta(1))+"%22and%20project="+PROJKEY+" and%20issuetype=Story"
            jiraRes=requests.get(URL,auth=(USER,PASSWORD), proxies=proxyDict)
            no=jiraRes.json()["total"]
        
        
       
            d={"Date":str(dt).split("-")[1]+"/"+str(dt).split("-")[2],"NO":no}
            calender_story.insert(0,d)
        URL=SERVER+"/rest/api/2/search?jql=issuetype=Defect and%20project="+PROJKEY
        nodefect=requests.get(URL,auth=(USER,PASSWORD), proxies=proxyDict).json()["total"]
        URL=SERVER+"/rest/api/2/search?jql=Sprint%20in%20openSprints()%20and%20issuetype=Story and%20project="+PROJKEY
        nostory=requests.get(URL,auth=(USER,PASSWORD), proxies=proxyDict).json()["total"]
        URL=SERVER+"/rest/api/2/search?jql=issuetype=Story and%20project="+PROJKEY+' and%20Status="To%20Do"'
        notodo_story=requests.get(URL,auth=(USER,PASSWORD), proxies=proxyDict).json()["total"]
        URL=SERVER+"/rest/api/2/search?jql=issuetype=Defect and%20project="+PROJKEY+' and%20Status="To%20Do"'
        notodo_defect=requests.get(URL,auth=(USER,PASSWORD), proxies=proxyDict).json()["total"]
        URL=SERVER+"/rest/api/2/search?jql=issuetype=Story and%20project="+PROJKEY+' and%20Status="In%20Progress"'
        noinpro_story=requests.get(URL,auth=(USER,PASSWORD), proxies=proxyDict).json()["total"]
        URL=SERVER+"/rest/api/2/search?jql=issuetype=Defect and%20project="+PROJKEY+' and%20Status="In%20Progress"'
        noinpro_defect=requests.get(URL,auth=(USER,PASSWORD), proxies=proxyDict).json()["total"]
        URL=SERVER+"/rest/api/2/search?jql=issuetype=Story and%20project="+PROJKEY+' and%20Status="Done"'
        nodone_story=requests.get(URL,auth=(USER,PASSWORD), proxies=proxyDict).json()["total"]
        URL=SERVER+"/rest/api/2/search?jql=issuetype=Defect and%20project="+PROJKEY+' and%20Status="Done"'
        nodone_defect=requests.get(URL,auth=(USER,PASSWORD), proxies=proxyDict).json()["total"]
    else:
        for d in range(0,7):
            dt=date.today() - timedelta(d)
            URL=SERVER+"/rest/api/2/search?jql=created%20%3E=%20%22"+str(dt)+"%22%20and%20created%20%3C=%20%22"+str(dt+timedelta(1))+"%22and%20project="+PROJKEY+" and%20issuetype=Defect and Sprint="+str(sprint_id)
            print(URL)
            jiraRes=requests.get(URL,auth=(USER,PASSWORD), proxies=proxyDict)
            no=jiraRes.json()["total"]
        
        
       
            d={"Date":str(dt).split("-")[1]+"/"+str(dt).split("-")[2],"NO":no}
            calender_defect.insert(0,d)
        for d in range(0,7):
            dt=date.today() - timedelta(d)
            URL=SERVER+"/rest/api/2/search?jql=created%20%3E=%20%22"+str(dt)+"%22%20and%20created%20%3C=%20%22"+str(dt+timedelta(1))+"%22and%20project="+PROJKEY+" and%20issuetype=Story and Sprint="+str(sprint_id)
            jiraRes=requests.get(URL,auth=(USER,PASSWORD), proxies=proxyDict)
            no=jiraRes.json()["total"]
        
        
       
            d={"Date":str(dt).split("-")[1]+"/"+str(dt).split("-")[2],"NO":no}
            calender_story.insert(0,d)
        URL=SERVER+"/rest/api/2/search?jql= issuetype=Defect and%20project="+PROJKEY+" and Sprint="+str(sprint_id)
        print("DEE",URL)
        nodefect=requests.get(URL,auth=(USER,PASSWORD), proxies=proxyDict).json()["total"]
        URL=SERVER+"/rest/api/2/search?jql= issuetype=Story and%20project="+PROJKEY+" and Sprint="+str(sprint_id)
        nostory=requests.get(URL,auth=(USER,PASSWORD), proxies=proxyDict).json()["total"]
        URL=SERVER+"/rest/api/2/search?jql=issuetype=Story and%20project="+PROJKEY+' and%20Status="To%20Do"'+" and Sprint="+str(sprint_id)
        notodo_story=requests.get(URL,auth=(USER,PASSWORD), proxies=proxyDict).json()["total"]
        URL=SERVER+"/rest/api/2/search?jql=issuetype=Defect and%20project="+PROJKEY+' and%20Status="To%20Do"'+" and Sprint="+str(sprint_id)
        notodo_defect=requests.get(URL,auth=(USER,PASSWORD), proxies=proxyDict).json()["total"]
        URL=SERVER+"/rest/api/2/search?jql=issuetype=Story and%20project="+PROJKEY+' and%20Status="In%20Progress"'+" and Sprint="+str(sprint_id)
        noinpro_story=requests.get(URL,auth=(USER,PASSWORD), proxies=proxyDict).json()["total"]
        URL=SERVER+"/rest/api/2/search?jql=issuetype=Defect and%20project="+PROJKEY+' and%20Status="In%20Progress"'+" and Sprint="+str(sprint_id)
        noinpro_defect=requests.get(URL,auth=(USER,PASSWORD), proxies=proxyDict).json()["total"]
        URL=SERVER+"/rest/api/2/search?jql=issuetype=Story and%20project="+PROJKEY+' and%20Status="Done"'+" and Sprint="+str(sprint_id)
        nodone_story=requests.get(URL,auth=(USER,PASSWORD), proxies=proxyDict).json()["total"]
        URL=SERVER+"/rest/api/2/search?jql=issuetype=Defect and%20project="+PROJKEY+' and%20Status="Done"'+" and Sprint="+str(sprint_id)
        nodone_defect=requests.get(URL,auth=(USER,PASSWORD), proxies=proxyDict).json()["total"]
    for d in range(0,7):
        dt=date.today() - timedelta(d)
        URL=SERVER+"/rest/api/2/search?jql=created%20%3E=%20%22"+str(dt)+"%22%20and%20created%20%3C=%20%22"+str(dt+timedelta(1))+"%22and%20project="+PROJKEY+" and issuetype != Epic AND resolution = Unresolved AND  (Sprint = EMPTY OR Sprint not in (openSprints(), futureSprints()))"
        
        jiraRes=requests.get(URL,auth=(USER,PASSWORD), proxies=proxyDict)
        no=jiraRes.json()["total"]
        
        
       
        d={"Date":str(dt).split("-")[1]+"/"+str(dt).split("-")[2],"NO":no}
        calender_backlogs.insert(0,d)
    
    URL=SERVER+"/rest/api/2/search?jql=project="+PROJKEY+" and issuetype != Epic AND resolution = Unresolved AND  (Sprint = EMPTY OR Sprint not in (openSprints(), futureSprints()))"
    noback=requests.get(URL,auth=(USER,PASSWORD), proxies=proxyDict).json()["total"]

    
    
    return calender_defect,calender_story,calender_backlogs,nodefect,noback,nostory,notodo_defect,notodo_story,noinpro_story,noinpro_defect,nodone_story,nodone_defect
        

   
    
            



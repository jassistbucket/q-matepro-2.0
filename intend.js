{"intents": [
        {"tag": "greeting",
         "patterns": ["Hi there", "How are you", "Is anyone there?", "Hello", "Good day","hey","Good morning","wassup","hi"],
         "responses": ["Hello", "Good to see you again", "Hi there, how can I help?"],
         "context": [""]
        },
        {"tag": "goodbye",
         "patterns": ["Bye", "See you later", "Goodbye", "Nice chatting to you, bye", "Till next time","bye"],
         "responses": ["See you!", "Have a nice day", "Bye! Come back again soon."],
         "context": [""]
        },
        {"tag": "thanks",
         "patterns": ["Thanks", "Thank you", "That's helpful", "Awesome, thanks", "Thanks for helping me"],
         "responses": ["Happy to help!", "Any time!", "My pleasure"],
         "context": [""]
        },
       
        
		  {"tag": "who_you",
         "patterns": ["Who are you??","Who is this?"],
         "responses": ["I am just a Virtual Assistant"],
         "context": [""]
        },
		 
		 
		 
		
		{"tag": "DEFECT_NO",
         "patterns": ["how many defects are there in the sprint","number of defects","total no of defects","count of defects","total defects","defect details","defect status","defects"],
         "responses": ["DEFECTNO"],
         "context": [""]
        },
			{"tag": "BACK_NO",
         "patterns": ["how many backlogs are there in the sprint","number of backlog","total no of backlog","count of backlogs","total backlogs","backlog details","all backlogs","backlogs","backlog status"],
         "responses": ["BACKNO"],
         "context": [""]
        },
		{"tag": "SPRINT_STAT",
         "patterns": ["sprint status","sprint details","show spirnt progress","track the sprint","sprint report","details sprint status","status of sprint","predict the progress","ontrack or offtrack","sprint is offtrack","sprint is ontrack"],
         "responses": ["SPRINTSTAT"],
         "context": [""]
        },
		{"tag": "ISSUE_TOT",
         "patterns": ["total issues","how many issue are there ?","count of issues in sprint","number of issues","total no of issue","total number of issues"],
         "responses": ["ISSUETOT"],
         "context": [""]
        }
		
		
		
		
   ]
}
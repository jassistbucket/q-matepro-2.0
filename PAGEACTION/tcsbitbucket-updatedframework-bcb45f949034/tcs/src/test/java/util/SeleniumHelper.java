package util;

import java.io.File;
import util.SeleniumDriver;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class SeleniumHelper{
	 
	static WebDriver driver;
	 public  static void getscreenshot(String name) throws Exception {		
		driver= SeleniumDriver.getDriver();
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		// The below method will save the screen shot in d drive with name
		// "screenshot.png"
		FileUtils.copyFile(scrFile, new File("./Screenshot/"+name+".png"));
	}
}

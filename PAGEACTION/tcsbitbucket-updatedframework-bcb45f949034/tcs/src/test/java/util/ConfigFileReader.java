package util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ConfigFileReader {
	private Properties properties;
	private final String propertyFilePath=System.getProperty("user.dir")+"//configs//Configuration.properties";
	
	public ConfigFileReader() {
		BufferedReader reader;
		try {
			reader= new BufferedReader(new FileReader(propertyFilePath));
			properties= new Properties();
			
			try {
				properties.load(reader);
				reader.close();
			}catch(IOException e) {
				e.printStackTrace();				
			}
		}catch(FileNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("Configuration properties is not found at "+propertyFilePath);
		}
	}

	public String getExcelPath(){
		String getExcelPath=properties.getProperty("testDataXLPath");
		if(getExcelPath!=null) return getExcelPath;
		else throw new RuntimeException("Excel data is not specified at Configuration.properties File");		
	}
	
	public String getExcelName() {
		String getExcelName=properties.getProperty("testDataXLName");
		if(getExcelName!=null) return getExcelName;
		else throw new RuntimeException("Excel data is not specified at Configuration.properties File");		
	}
	
	public String getReportPath() {
		String getReportPath=properties.getProperty("reportFilePath");
		if(getReportPath!=null) return getReportPath;
		else throw new RuntimeException("Report Path is not specified at Configuration.properties File");		
	}
	
	public String getXLReportPath() {
		String getXLReportPath=properties.getProperty("reportXLFilePath");
		if(getXLReportPath!=null) return getXLReportPath;
		else throw new RuntimeException("XL Report Path is not specified at Configuration.properties File");		
	}
	
	public String getURLPath() {
		String getURLPath=properties.getProperty("urlPath");
		if(getURLPath!=null) return getURLPath;
		else throw new RuntimeException("URL Path is not specified at Configuration.properties File");		
	}
	
	public long getImplicitlyWait() {
		String implicitlyWait=properties.getProperty("implicitlyWait");
		if(implicitlyWait!=null) return Long.parseLong(implicitlyWait);
		else throw new RuntimeException("implicitlyWait is not specified at Configuration.properties File");		
	}
	
	public String getApplicationURL() {
		String url=properties.getProperty("url");
		if(url!=null) return url;
		else throw new RuntimeException("url not specified at Configuration.properties File");		
	}
	
	public String getXLSheetName(String Xltype) {
		String getXLSheetName=properties.getProperty("DataSheetBlue");
		if(Xltype.equalsIgnoreCase("gold")) {
			getXLSheetName=properties.getProperty("DataSheetGold");
		}
		if(getXLSheetName!=null) return getXLSheetName;
		else throw new RuntimeException("Excel Sheet Name is not specified at Configuration.properties File");		
	}

}

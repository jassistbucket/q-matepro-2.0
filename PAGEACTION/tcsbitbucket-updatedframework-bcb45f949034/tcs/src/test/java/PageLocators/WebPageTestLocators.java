package PageLocators;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import util.SeleniumDriver;

public class WebPageTestLocators {
	
	@FindBy(xpath="//h2[@class='cufon-dincond_black']")
	public WebElement h2_header;

	public WebPageTestLocators()
	{
		PageFactory.initElements(SeleniumDriver.getDriver(),this);
	}

}

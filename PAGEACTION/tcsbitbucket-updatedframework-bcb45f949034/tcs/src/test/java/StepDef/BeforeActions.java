package StepDef;

import java.net.MalformedURLException;

import org.testng.log4testng.Logger;

import cucumber.api.Scenario;
import cucumber.api.java.Before;
import util.SeleniumDriver;

public class BeforeActions {
	Logger log=Logger.getLogger(BeforeActions.class);
	public static Scenario scenario;
	
	@Before
	public void setUp(Scenario scenario) throws MalformedURLException {
		log.info("Setting up driver...");
		SeleniumDriver.setUpDriver();
		log.info("Driver initialized...");
		BeforeActions.scenario=scenario;
	}
}

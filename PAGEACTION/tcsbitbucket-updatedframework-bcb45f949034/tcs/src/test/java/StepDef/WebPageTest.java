package StepDef;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import PageActions.WebPageTestActions;
import cucumber.api.java.en.Given;
import util.SeleniumDriver;
import util.SeleniumHelper;


public class WebPageTest  {
	
	WebPageTestActions pageActions = new WebPageTestActions();
	@Given("^user opens a \"([^\"]*)\" website$")
	public void user_opens_a_website(String url) throws Throwable {
		SeleniumDriver.openPage(url);
		SeleniumHelper.getscreenshot("Test");
	}

	@When("^search for \"([^\"]*)\" and verify$")
	public void search_for_and_verify(String text) throws Throwable {
		String h2 = pageActions.get_h2Header();
		Assert.assertEquals(h2, text);
	}

}

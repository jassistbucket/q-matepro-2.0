import numpy as np
import pandas as pd
#import pika
import nltk
import dep_parse2
from whoosh.index import create_in
from whoosh.fields import *
import os.path
from nltk.corpus import stopwords 
from nltk.tokenize import word_tokenize
from whoosh.index import open_dir
import random
from whoosh.analysis import StemmingAnalyzer
import string
from whoosh.qparser import QueryParser,MultifieldParser
import requests
import json
#from sqlalchemy import create_engine
#import mysql.connector
import urllib
from difflib import SequenceMatcher
from whoosh.query import FuzzyTerm
import json
response_slug=[]
branches=[]

stop_words = set(stopwords.words('english')) 
class MyFuzzyTerm(FuzzyTerm):
     def __init__(self, fieldname, text, boost=1.0, maxdist=2, prefixlength=1, constantscore=True):
         super(MyFuzzyTerm, self).__init__(fieldname, text, boost, maxdist, prefixlength, constantscore)

def get_absolute(query,duplicates):
    searcher = ix.searcher()
    result=[]

    qp = MultifieldParser(["content","filename"], schema=ix.schema,termclass=MyFuzzyTerm)
    q = qp.parse(query)
    with ix.searcher() as s:
            print('searching')
            results = s.search(q,limit=None)
            print(results.scored_length())
            print(results.scored_length())
            if results.scored_length() == 0:
                    
                    
                    FNAM="No Match Found"
                    CODE="No Match Found"
                    PTH="No Match Found"
                    REPO="No Match Found"
                    d={'Repo':REPO,'File Name': FNAM,'Path':PTH,'Code':CODE,'No_Result':str(results.scored_length())}
                    result.append(d)
                    return result
            else:
                for hit in results:
                        REPO=str(set((data.repo[data.key == hit['key']]).values))
                        REPO=REPO.strip(" '{}'")
                        FNAM = str(set((data.filename[data.key == hit['key']]).values))
                        FNAM=FNAM.strip(" '{}'")
                        CODE = str(set((data.content[data.key == hit['key']]).values))
                        CODE=CODE.strip(" '{}'")
                        #CODE=CODE.replace('\\n','\n')
                        CODE="Scenario:"+FNAM+'\n'+CODE
                        PTH = str(set((data.path[data.key == hit['key']]).values))
                        
                        PTH=PTH.strip(" '{}'")
                        
                        
                        
                        d={'File Name': FNAM,'Path':PTH,'Code':CODE,'Repo':REPO,'No_Result':str(results.scored_length())}
                        if CODE not in duplicates:
                            duplicates.append(CODE)
                            result.append(d)

                return result
    
def get_branches(ACCESS_TOKEN):

    src1='https://api.bitbucket.org/2.0/repositories/cardsproject2019'+ACCESS_TOKEN
    fp1 = requests.get(src1,auth=('shovona.sarkar@gmail.com','password'),verify=False)
    mystr1=json.loads(fp1.text)
    #print(mystr1)
    response_i=mystr1["values"]
    for r in response_i:
        response_slug.append(r['slug'])
    #print(response_slug)
    for k in response_slug:
        print(k)
        src2='https://api.bitbucket.org/2.0/repositories/cardsproject2019/'+k+'/refs/branches'+ACCESS_TOKEN
        print(src2)
        fp2 = requests.get(src2,auth=('shovona.sarkar@gmail.com','password'),verify=False)
        mystr2=json.loads(fp2.text)
        response_i2=mystr2['values']
        #print(response_i2)
        for r in response_i2:
            #print('ss',r['name'])
            d={k:r['name']}

            branches.append(d)
    return branches
def get_dataframe():
    
    import requests
    sj=''
    import json
    ACCESS_TOKEN='?access_token=N-1lS89sVJ66zdnPG6XV9JYshfBPGCGEIIf4Hrx7o-O64hRxbRxzFsCk787wDrzTc_WYyJYexUQNlvXVVQ%3D%3D'

    df=pd.DataFrame()
    b=get_branches(ACCESS_TOKEN)
    #print(b)

    for i in b:
            for key,values in i.items():
                #print(key,values)
                src='https://api.bitbucket.org/1.0/repositories/cardsproject2019/'+key+'/src/'+values+'/'+ACCESS_TOKEN
                fp = requests.get(src,auth=('shovona.sarkar@gmail.com','password'),verify=False)
                mystr=(fp.text)
                mystr=json.loads(mystr)
                #print(mystr)
                direc=mystr['directories']
                #print(direc)
                if not direc:
                    pass
                else:
                    for u in direc:
                        sj='https://api.bitbucket.org/1.0/repositories/cardsproject2019/'+key+'/src/'+values+'/'+u+ACCESS_TOKEN
                        fj = requests.get(sj,auth=('shovona.sarkar@gmail.com','password'),verify=False)
                        my=(fj.text)
                        my=json.loads(my)
                        w=(my['files'])
                        for folder in w:

                            sr='https://api.bitbucket.org/1.0/repositories/cardsproject2019/'+key+'/src/'+values+'/'+(folder['path'])+ACCESS_TOKEN
                            print(sr)
                            fx = requests.get(sr,auth=('shovona.sarkar@gmail.com','password'),verify=False)
                            myx=(fx.text)
                            myx=json.loads(myx)
                            code1=(myx['data'])
                            filename1=(myx['path'])
                            pth1=sr.replace("api.bitbucket.org/1.0/repositories", "bitbucket.org")
                            #print(pth)
                            d1={'filename':filename1,'code':code1,'path':pth1}
                            df = df.append(d1, ignore_index=True)
                    print(df['filename'])
                mystr=(mystr['files'])

                for mystring in mystr:

                    src2='https://api.bitbucket.org/1.0/repositories/cardsproject2019/'+key+'/src/'+values+'/'+(mystring['path'])+ACCESS_TOKEN
                    #print(src2)
                    fp2 = requests.get(src2,auth=('shovona.sarkar@gmail.com','password'),verify=False)
                    mystr2=(fp2.text)
                    mystr2=json.loads(mystr2)
                    code=(mystr2['data'])
                    filename=(mystr2['path'])
                    pth=src2.replace("api.bitbucket.org/1.0/repositories", "bitbucket.org")
                    #print(pth)
                    d={'filename':filename,'code':code,'path':pth}
                    df = df.append(d, ignore_index=True)
    data = df
    print(data['filename'])
    data = data.fillna(' ')
    data['filename'] = data['filename'].astype(str)
    data['code'] = data['code'].astype(str)
    data['path'] = data['path'].astype(str)
    data=data[['filename','code','path']]
    conn,engine=db_connection()
    data.to_sql(name='bitbot', con=engine, if_exists = 'replace', index=False)

    print("Data Read DB")
    #return data
def tem_featuredata(conn,engine):
    df=pd.DataFrame()
    Data=pd.read_excel('D://Soorajit//Jira2019//Jira 2019 app//FeatureData3.xls')
    Data=Data[['S_NAME','S_CON','S_PATH']]
    for index, row in Data.iterrows():
        scenario_name=row['S_CON'].split('\n')
        scenario_name=scenario_name[0]
        print('QQQQQQQQQQQQ\n\n',scenario_name)
        scenario_con=row['S_CON'].replace(scenario_name,'')
        scenario_repo=(row['S_PATH'].split('/src/master/'))
        scenario_repo=scenario_repo[0]
        scenario_repo=scenario_repo.split('/')[-1]
        scenario_path=row['S_PATH']
        d={'repo':scenario_repo,'filename':scenario_name,'content':scenario_con,'path':scenario_path}
        df = df.append(d, ignore_index=True)
    df.to_sql(name='featuredata', con=engine, if_exists = 'replace', index=False)
    print('INSERTED')
def read_data():
    
    data = pd.read_excel("FeatureData2.xlsx")
    data['key']=data.index.astype(str)
    data['repo'] = data['repo'].astype(str)
    data['filename'] = data['filename'].astype(str)
    #data['key'] = data['key'].str.lower()
    data['content'] = data['content'].astype(str)
    
    data['path'] = data['path'].astype(str)
    
	
    #print(data['content'])
    #data = preprocess_status(data)
    print(" [X] data read succesfully...")
    print()
    print(data.info())

    return data
def read_data_scenario(conn,engine):
    df=pd.DataFrame(columns=['E'])
    
    C=0
    NEW=[]
    O=[]
    query = engine.execute("select distinct repo,filename,content,path from featurebot")
    data = pd.DataFrame(query.fetchall())
    data.columns = query.keys()

    conn.close()
    engine.dispose()
    
    data['repo'] = data['repo'].astype(str)
    data['filename'] = data['filename'].astype(str)
    #data['key'] = data['key'].str.lower()
    data['content'] = data['content'].astype(str)
    data['content'] = data['content'].str.lower()
    data['path'] = data['path'].astype(str)
    
    for content in data['content']:
        print(content)
        scenario_flag=0
        scenario=''
        C=C+1
        for line in content.split('\n'):
            if 'scenario' in line or '{"type":"error","error":{"message"' in line:
                scenario_flag=1
                #print('\n\naaaaaa,,,,,',line)
            if scenario_flag==1:
                scenario=scenario+line
        print('scenario is\n\n\n',scenario)
        NEW.append(scenario)
    df=pd.DataFrame(NEW,columns=['E'])
    (data['content'])=(df['E'])
        
  
    data_scene=data
    return data_scene

def create_schema():

    schema = Schema(key=TEXT(stored=True),repo=TEXT(stored=True),filename=TEXT(stored=True), content=TEXT(analyzer=StemmingAnalyzer()), path=TEXT(analyzer=StemmingAnalyzer()))

    if not os.path.exists("featureindex"):
        os.mkdir("featureindex")
    ix = create_in("featureindex", schema)
    print("Schema Created")
    return None
def create_index(data):

    ix = open_dir("featureindex")
    writer = ix.writer()

    for index, row in data.iterrows():
        writer.add_document(key=row['key'],repo=row['repo'],filename=row['filename'], content=row['content'], path = row['path'])

    writer.commit()
    print("INDEX CREATED")

    return ix            
def get_results(query):
    duplicates=[]
    query2=''
    COMB=[]
    searcher = ix.searcher()


    qp = MultifieldParser(["content","filename"], schema=ix.schema)

    ToDisplay=''

    result=[]
    d={}
    if "[" and "]" in query:
        query=query[query.find("[")+1:query.find("]")]
        q = qp.parse(query)
        with ix.searcher() as s:
            print('searching')
            results = s.search(q,limit=None)
            print(results.scored_length())
            print(results.scored_length())
            if results.scored_length() == 0:
                    FNAM="No Match Found"
                    CODE="No Match Found"
                    PTH="No Match Found"
                    REPO="No Match Found"
                    d={'Repo':REPO,'File Name': FNAM,'Path':PTH,'Code':CODE,'No_Result':str(results.scored_length())}
                    result.append(d)
                    
                    return result
            else:
                for hit in results:
                        REPO=str(set((data.repo[data.key == hit['key']]).values))
                        REPO=REPO.strip(" '{}'")
                        FNAM = str(set((data.filename[data.key == hit['key']]).values))
                        FNAM=FNAM.strip(" '{}'")
                        CODE = str(set((data.content[data.key == hit['key']]).values))
                        CODE=CODE.strip(" '{}'")
                        #CODE=CODE.replace('\\n','\n')
                        CODE="Scenario:"+FNAM+'\n'+CODE
                        PTH = str(set((data.path[data.key == hit['key']]).values))
                        
                        PTH=PTH.strip(" '{}'")
                        
                        
                        
                        d={'File Name': FNAM,'Path':PTH,'Code':CODE,'Repo':REPO,'No_Result':str(results.scored_length())}
                        if CODE not in duplicates:
                            duplicates.append(CODE)
                            result.append(d)

                return result
    else:
                

        q = qp.parse(query)
        print('parsing')

        with ix.searcher() as s:
            print('searching')
        
            
            
            results = s.search(q,limit=None)
            print(results.scored_length())
            print(results.scored_length())
            if results.scored_length() == 0:
            
  
                word_tokens = word_tokenize(query) 
  
                filtered_sentence = [w for w in word_tokens if not w in stop_words] 
  
                filtered_sentence = [] 
  
                for w in word_tokens: 
                  if w not in stop_words: 
                      filtered_sentence.append(w) 
            
                filtered_text=' '.join(filtered_sentence)
                filtered_text=filtered_text.translate(str.maketrans('', '', string.punctuation))
                nltk_tokens = nltk.word_tokenize(filtered_text)
                BIGRAM_TEXT=(list(nltk.bigrams(nltk_tokens)))
                for b in BIGRAM_TEXT:
                  if b[0]==b[1]:
                      print('qqqqq',b)
                      pass
                  elif ' '.join(b[::-1]) in COMB:
                    
                    pass
                  else:
                  
                      _text=' '.join(b)
                      COMB.append(_text)
                print(COMB)
                for c in COMB:
                  q = qp.parse(c)
                  results = s.search(q,limit=None)
                  if results.scored_length() == 0:
                    print(c)
                  else:
                    print('bigram found',c)
                    cc=c.split(' ')
                
                
                    for hit1 in results:
                
                          REPO=str(set((data.repo[data.key == hit1['key']]).values))
                          REPO=REPO.strip(" '{}'")
                          FNAM = str(set((data.filename[data.key == hit1['key']]).values))
                          FNAM=FNAM.strip(" '{}'")
                          CODE = str(set((data.content[data.key == hit1['key']]).values))
                          CODE=CODE.strip(" '{}'")
                          CODE="Scenario:"+CODE
                          #CODE=CODE.replace('\\n','\n')
                          PTH = str(set((data.path[data.key == hit1['key']]).values))
                        
                          PTH=PTH.strip(" '{}'")
                          print('\n\nPTH>>>>>\n\n',PTH)
                    
                        
                        
                          d={'File Name': FNAM,'Path':PTH,'Code':CODE,'Repo':REPO,'No_Result':str(results.scored_length())}
                          if CODE not in duplicates:
                            duplicates.append(CODE)
                            result.append(d)
                if result!=[]:
                  return result
                else:
                    try:
                      
                      d1=dep_parse2.main_process(query)
                      
                      qu=d1['Beta']
                      TYPE=d1["TYPE"]
                      TYPE_CON=d1["TYPE_CON"]
                      print('quuuu',qu)
                      if qu=='scen':
                  
                          return 'scen'
                      if TYPE=="SIMPLE":
                        print("SIMPLE")
                        if len(qu)>1:
                            query2=' OR '.join(qu)
                        else:
                            try:
                                query2=qu[0]
                            except IndexError:
                                query2='INDEXERROR'
                    
                      else:
                        print("COMPOUND")
                        query2=query2=' OR '.join(qu)
                    except:
                        query2='INDEXERROR'
            
                    q1 = qp.parse(query2)
                    results2 = s.search(q1, limit = None)
                    if results2.scored_length() == 0 or query2=='INDEXERROR' :
                        
                        
                        corrected = s.correct_query(q1,query2)
                        print('corrected',corrected)
                        if corrected.query != q1:
                                corr=query.replace(query2,corrected.string)
                                print('corrrrrrrrr>>>>>>>>',corr,query)
                                if (corr==query):
                                    print('qqq')
                                    pass
                                else:
                                    #for spelling mistake of user_query suggesting user correct word
                                    print('\n\n')
                                    FNAM = "Did you mean? "+corr
                                    CODE = "No match found."
                                    PTH = "No match found."
                                    result=[]
                                    REPO= " No match found."
                                    d={'Repo':REPO,'File Name': FNAM,'Path':PTH,'Code':CODE,'No_Result':str(results2.scored_length())}
                                    result.append(d)
                                return result
                        else:
                            RESPONSE=get_absolute(query,duplicates)
                            
                            return RESPONSE

                    else:
                        print('results=\n',results2)
                        print('in ok')
                

                        for hit in results2:
                            REPO=str(set((data.repo[data.key == hit['key']]).values))
                            REPO=REPO.strip(" '{}'")
                            FNAM = str(set((data.filename[data.key == hit['key']]).values))
                            FNAM=FNAM.strip(" '{}'")
                            CODE = str(set((data.content[data.key == hit['key']]).values))
                            CODE=CODE.strip(" '{}'")
                            #CODE=CODE.replace('\\n','\n')
                            CODE="Scenario:"+FNAM+'\n'+CODE
                            PTH = str(set((data.path[data.key == hit['key']]).values))
                        
                            PTH=PTH.strip(" '{}'")
                        
                        
                        
                            d={'File Name': FNAM,'Path':PTH,'Code':CODE,'Repo':REPO,'No_Result':str(results2.scored_length())}
                            if CODE not in duplicates:
                                duplicates.append(CODE)
                                result.append(d)

                        return result
            
            
              
            else:
                print('results=\n',results)
            
            

                #result=[]
                for hit1 in results:
                
                        REPO=str(set((data.repo[data.key == hit1['key']]).values))
                        REPO=REPO.strip(" '{}'")
                        FNAM = str(set((data.filename[data.key == hit1['key']]).values))
                        FNAM=FNAM.strip(" '{}'")
                        CODE = str(set((data.content[data.key == hit1['key']]).values))
                        CODE=CODE.strip(" '{}'")
                        CODE="Scenario:"+FNAM+'\n'+CODE
                        #CODE=CODE.replace('\\n','\n')
                        PTH = str(set((data.path[data.key == hit1['key']]).values))
                        
                        PTH=PTH.strip(" '{}'")
                        print('\n\nPTH>>>>>\n\n',PTH)
                    
                        
                        
                        d={'File Name': FNAM,'Path':PTH,'Code':CODE,'Repo':REPO,'No_Result':str(results.scored_length())}
                        if CODE not in duplicates:
                            duplicates.append(CODE)
                            result.append(d)

                return result

#conn,engine=db_connection()

data=read_data()

#data=data[~data.content.str.contains('{"type":"error","error":{"message"')]

#data_scen=read_data_scenario(conn,engine)

#print(data_scen["content"].head)
#data_scen=data_scen[~data_scen.content.str.contains('{"type":"error","error":{"message"')]
#create_schema()
ix=create_index(data)

#ix = open_dir("featureindex")




#******************************IMPORTING MODULES******************************************#

import spacy
import en_core_web_sm
import re
import json
import string
from stop_words import get_stop_words
#*************************END OF IMPORT************************************************#

nlp = en_core_web_sm.load()
mydic_login=['logon','signin','login','log','sign']
mydic_dash=['report','dashboard']
mydic_pay=['payments','payment']


def remove_stop_punc(Ch):
    list_of_words=[]
    
    stop_words = get_stop_words('en')
    stop_words.extend(['related','file','features','files','scenario','scenarios','feature','can','could','would','will','please','inside','.', ',', '"', "'", '?', '!', ':', ';', '(', ')', '[', ']', '{', '}','~']) # stopwords and punc
    
    list_of_words = [str(i).lower() for i in Ch if str(i).lower() not in stop_words]
           
                
    
    print('LIST\n\n\n\n',list_of_words)
    return list_of_words

    
def extract_simple_keywords(text,l):
    
    doc = nlp(text)
    Ch=[]
    Children=[]
    


    #************************EXTRACTING CHILDREN OF ROOT DEPENDENCY********************#
    for i in l:
        if(i["Dep"]=="ROOT"):
            Ch=list(i["Children"])
    #**********************EXTRACTING CHILDREN OF REMAINING CHILDREN***********************#

    for i in l:
        
    
    
        for m in Ch:
        
            if(i["Text"]==m):
            
                if(i['Children'])==[]:
                    print('POSSSSSS',i['Pos'])
                    #if(l[i]["Pos"]=='NOUN' or l[i]["Pos"]=='PROPN'):
                
                    Children.append(m)
                
                else:
                    #print(Ch)
                
                
                    for o in (i["Children"]):
                        print(o)
                        if(i["Text"]==o):#preventing program to go infinite loop while duplicate words are present
                    
                            print('duplicate')
                            break
                        else:
                            Children.append(o)
                    
    #print(Children)
                          
                           
    for p in l:
    
        for u in Children:
            #print('qq')
            if(p["Text"]==u):
           
                
            
                for child in (p["Children"]):
                    if(p["Text"]==child): #preventing program to go infinite loop while duplicate words are present
                        break
                    else:
                        Children.append(child)
    
                    
    #*************************POST-PROCESSING*****************************************#
    Children=remove_stop_punc(Children)
    Children = [''.join(c for c in s if c not in string.punctuation) for s in Children]
    Children= [s for s in Children if s]
    if Children==[]:
        Children=[h for h in Ch]
        Children=remove_stop_punc(Children)
        Children = [''.join(c for c in s if c not in string.punctuation) for s in Children]
        Children= [s for s in Children if s]
    b = filter(None, Children)
    b=(list(b))
    #***********************END OF POST-PROCESSING***********************************#
    print(b)
    b2=[]
    b2=b.copy()
    '''
    if(len(b)>1):
        for token in doc:
            for h in b:
            
                if(str(token.text)==h):
                    print(token.text)
                
                    if(token.head.text) in b:
                        pass                   
                    else:
                        
                        b.remove(h)
                        if b==[]:
                            print('nothing')
                            if(token.dep_=='pobj' or token.dep_=='amod'):
                                print('hello')
                                print(token.text)
                                if(token.head.text=='of' or token.head.text=='about' or token.head.text=='on' or token.head.text=='to'):
                                        
                                        b.append(b2[-1])
                                else:
                                    b.append(b2[0])
                            else:
                                b.append(b2[-1])
                        else:
                            pass'''
                            
                
    
    if b==[]:
        print("SORRY!")
        for token in doc:
            
            if token.dep_=='ROOT':
                print(token.pos_)
               
                b.append(token.text)
                break
               
                    
        
    '''
    for M in b:
        if M=='login' or M=='logon' or M=='signin' or M=='log' or M=='sign':
            
            for dic in mydic_login:
               
                b.append(dic)
            break
        elif M=='dashboard' or M=='report' :
            
            for dic1 in mydic_dash:
                
                
                b.append(dic1)
            break
        elif M=='payments' or M=='payment' :
            
            for dic2 in mydic_pay:
                
                
                b.append(dic2)
            break
        else:
            pass
  
    b=set(b)       
    b=(list(b))'''
    print(b)
    return b
def extract_compund_keywords(text):
    Children=[]
    b=[]
    
    doc = nlp(text)
    for token in doc:
        print('\n\nCONJU',list(token.conjuncts),token.left_edge)
        if not (token.conjuncts)==():
            print('texttttttttt',token.left_edge,token.right_edge)
            if str(token.left_edge) not in Children:
                #print(token.pos_)
                if token.pos_=="NOUN" or token.pos_=="PROPN":
                    Children.append(str(token.left_edge))
            else:
                pass

    #*************************POST-PROCESSING*****************************************#
    Children=remove_stop_punc(Children)
    Children = [''.join(c for c in s if c not in string.punctuation) for s in Children]
    Children= [s for s in Children if s]
    b = filter(None, Children)
    b=(list(b))
    print('com b',b)
    #*************************END OF POST-PROCESSING*****************************************#
    if len(b)==0:
        LEF=0
        RIG=0
        print('COM SORRY')
        Q=[{"TEXT":token.text,"DEP":token.dep_,"POS":token.pos_,"POSITION":token.i} for token in doc]
        print(Q)
        for idx, val in enumerate(Q):
            if val["DEP"]=='cc':
                LEF=RIG=idx #GETTING THE INDEX OF AND
        print('FI\n\n\n',LEF)
        for u in Q: 
            LEF=LEF-1
            if(Q[LEF]["POS"]=='NOUN' or Q[LEF]["POS"]=='PROPN'):
                b.append(Q[LEF]["TEXT"]) # APPENDING IMMEDIATE LEFT SIDE NOUN
                break
            else:
                pass
        for u in Q:
            RIG=RIG+1

            print('FQQ',Q[RIG]["POS"],Q[RIG]["TEXT"])
            if(Q[RIG]["POS"]=='NOUN' or Q[RIG]["POS"]=='PROPN'):
                b.append(Q[RIG]["TEXT"]) # APPENDING IMMEDIATE RIGHT SIDE NOUN
                break
            else:
                pass
    


    print(b)           
    return b

def main_process(text):
    TYPE=''
    TYPE_CON=''
    beta=[]
    l=[]
    FLAG_SIMPLE=0
    
    
    #*************************PRE-PROCESSING**************************************#
    if ',' in text:
        text=text.replace(',',' and ')
    print(text)
    if 'search' in text:
        text=text.replace('search','show')
    
    
    text=text.rstrip()
    text=text.lower() #To Lower Case
    text=''.join(i for i in text if ord(i)<128)#Removing NON-ASCII Char
    text=re.sub(r'[^\x00-\x7F]',' ', text) #Replacinng White Space for Removed ASCII Char
    text=re.sub(' +', ' ', text) #Removing Extra Spaces from Sentence
    
    #*************************TOKENIZATION**************************************#

    doc = nlp(text)


    for token in doc:
   
        print("{0}/{1} <--{2}-- {3}/{4}".format(
           token.text, token.tag_, token.dep_, token.head.text,token.is_stop))
    
        d={"Text":token.text,"Pos":token.tag_,"Head":token.head.text,"Dep":token.dep_,"Children":[str(child) for child in token.children]}
        l.append(d)
    print(l)
    for i in l:
        if(i["Dep"]=='cc'):
            TYPE_CON=i["Text"]
            FLAG_SIMPLE=1
            break
        
      
            
    print(FLAG_SIMPLE)        
    if FLAG_SIMPLE!=1:
        beta=extract_simple_keywords(text,l)
        TYPE='SIMPLE'
    else:
        print('Compund Sentence Found')
        beta=extract_compund_keywords(text)
        TYPE='COMPOUND'
    diction={"Beta":beta,"TYPE":TYPE,"TYPE_CON":TYPE_CON}    
    return diction
    
        
       

    
    


  

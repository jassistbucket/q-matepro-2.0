var xpath_dataRec=[];
$('#stop').hide();
var watch = (function(){
    var timer = document.getElementById("timer");
    var stop = document.getElementById("stop");
    var reset = document.getElementById("reset");
    var time = "00:00"
    var seconds = 0;
    var minutes = 0;
    var t;
  
    timer.textContent = time;
  
    function buildTimer () {
      seconds++;
          if (seconds >= 60) {
              seconds = 0;
              minutes++;
              if (minutes >= 60) {
                  minutes = 0;
                  seconds = 0;
              }
          }
      timer.textContent = (minutes < 10 ? "0" + minutes.toString(): minutes) + ":" + (seconds < 10 ? "0" + seconds.toString(): seconds);
          try{

      $.getJSON('/QRec', {
				  
        track:"Hello",
     }, function(data) {
         var row_data=data.data
         if(data.data==='NOWINDOW'){
          $.notify("No valid Tabs are launched.", "warn");
         }
         if(data.data==="CLOSE"){
          $("#stop").trigger("click");
          $.getJSON('/QRec_Stop', {
				  
            track:"Hello",
         }, function(data) {
           ///
           
    
    
         });


         }
         else{
         
         if(row_data!==null&&row_data.length!==0){
            row_data=JSON.parse(row_data);
            console.log(row_data)
            var table = document.getElementById('myTable').getElementsByTagName('tbody')[0];
            var row = table.insertRow(-1);
            ///var cell1 = row.insertCell(-1);
            
            row.innerHTML = '<tr><td class="text-left">'+row_data["EVENT"]+'</td><td class="text-left">'+row_data["XPATH"]+'</td></tr>'
            xpath_dataRec.push(row_data);
         }

        }


     });}
     catch(err){
      $.notify("Q-Recorder says:"+err, "error");
      console.log(err);
      $("#stop").trigger("click");
      
      $.getJSON('/QRec_Stop', {
				  
        track:"Hello",
     }, function(data) {


     });

     }
    }
    function stopTimer () {
      stop.addEventListener("click", function(){
        clearTimeout(t);
        timer.textContent = time;
        seconds = 0; minutes = 0;
        $('#stop').hide();
        $('#start').show();
        
        $.getJSON('/QRec_Stop', {
				  
          track:"Hello",
       }, function(data) {
         ///
  
  
       });
        
      })
     
    }
    function startTimer () {
      start.addEventListener("click", function(){
        clearTimeout(t);
        t = setInterval(buildTimer,1000);
        $('#start').hide();
        $('#stop').show();
      });
      
    }
    
    return {
      start: startTimer(),
      stop: stopTimer()
      
    };
  })()
  window.onscroll = function() {myFunction()};

var navbar = document.getElementById("navbar");
var sticky = navbar.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    navbar.classList.add("sticky")
  } else {
    navbar.classList.remove("sticky");
  }
}

window.addEventListener("beforeunload", function (e) {
    var confirmationMessage = "\o/";
  
    (e || window.event).returnValue = confirmationMessage; //Gecko + IE
    return confirmationMessage;                            //Webkit, Safari, Chrome
  });


  function convertArrayOfObjectsToCSVRec(args) {
    var result, ctr, keys, columnDelimiter, lineDelimiter, data;

    data = args.data || null;
    if (data == null || !data.length) {
        return null;
    }

    columnDelimiter = args.columnDelimiter || ',';
    lineDelimiter = args.lineDelimiter || '\n';

    keys = Object.keys(data[0]);

    result = '';
    result += keys.join(columnDelimiter);
    result += lineDelimiter;

    data.forEach(function(item) {
        ctr = 0;
        keys.forEach(function(key) {
            if (ctr > 0) result += columnDelimiter;

            result += item[key];
            ctr++;
        });
        result += lineDelimiter;
    });

    return result;
}

function downloadCSVRec(args) {
    var data, filename, link;

    var csv = convertArrayOfObjectsToCSVRec({
        data: xpath_dataRec
    });
    if (csv == null) return;

    filename = args.filename || 'export.csv';

    if (!csv.match(/^data:text\/csv/i)) {
        csv = 'data:text/csv;charset=utf-8,' + csv;
    }
    data = encodeURI(csv);

    link = document.createElement('a');
    link.setAttribute('href', data);
    link.setAttribute('download', filename);
    link.click();
}
function downloadXlsRec(){


  downloadCSVRec({ filename: "AllSteps.csv" });
}

//Get the button
var mybutton = document.getElementById("myBtn");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
function selectMetric(e) {
    e.preventDefault();
    var metric = $(e.currentTarget).attr('data-metric');
    var metricSelector = '[data-metric="' + metric + '"]';
  
    // set selected link
    $('.side .selected').removeClass('selected');
    $('.side .links a' + metricSelector).addClass('selected');
  
    // show proper stats
    $('.side .stats ul').hide();
    $('.side .stats ul' + metricSelector).show();
  
    // activate proper graph
    var $wrapper = $('.main article' + metricSelector).parent();
    var isActive = $wrapper.hasClass('active');
    if (!isActive) {
      $wrapper
        .addClass('active')
        .siblings().removeClass('active');
    }
  }
  
  function checkKey(e) {
    if (e.keyCode === 13) {
      // hit enter
      selectMetric(e);
    }
  }
  
  $('.side .links a').on('click', selectMetric);
  $('.main article').on({
    'click': selectMetric,
    'keyup': checkKey
  });
  $("#loader").hide();
  $('#selectSprint').change(function() {
    $("#HOMEbody").fadeTo("slow",0.6);
    $("#loader").show();
    $.getJSON('/get_by_sprint', {
      sprint_id: $(this).val(),
    }, function(data) {
      
     console.log(data.calender_defect);
     var stories=data.calender_story;
     var defects=data.calender_defect;
     var backlogs=data.calender_backlogs;
     chart.options.data[0].dataPoints=[
      { label: backlogs[0]["Date"], y: backlogs[0]["NO"] },
          { label: backlogs[1]["Date"], y: backlogs[1]["NO"]},
          { label: backlogs[2]["Date"], y: backlogs[2]["NO"] },
          { label: backlogs[3]["Date"], y: backlogs[3]["NO"] },
          { label: backlogs[4]["Date"], y: backlogs[4]["NO"] },
          { label: backlogs[5]["Date"], y: backlogs[5]["NO"] },
          { label: backlogs[6]["Date"], y: backlogs[6]["NO"] }
      ]

      chart2.options.data[0].dataPoints=[
        { label: defects[0]["Date"], y: defects[0]["NO"] },
            { label: defects[1]["Date"], y: defects[1]["NO"]},
            { label: defects[2]["Date"], y: defects[2]["NO"] },
            { label: defects[3]["Date"], y: defects[3]["NO"] },
            { label: defects[4]["Date"], y: defects[4]["NO"] },
            { label: defects[5]["Date"], y: defects[5]["NO"] },
            { label: defects[6]["Date"], y: defects[6]["NO"] }
        ]

        chart3.options.data[0].dataPoints=[
          { label: stories[0]["Date"], y: stories[0]["NO"] },
              { label: stories[1]["Date"], y: stories[1]["NO"]},
              { label: stories[2]["Date"], y: stories[2]["NO"] },
              { label: stories[3]["Date"], y: stories[3]["NO"] },
              { label: stories[4]["Date"], y: stories[4]["NO"] },
              { label: stories[5]["Date"], y: stories[5]["NO"] },
              { label: stories[6]["Date"], y: stories[6]["NO"] }
          ]
          chart.render();
          chart2.render();
          chart3.render();
          document.getElementById("nostory").innerHTML=data.nostory;
          document.getElementById("nodefect").innerHTML=data.nodefect;
          document.getElementById("noback").innerHTML=data.noback;
          document.getElementById("notodo_defect").innerHTML=data.notodo_defect;
          document.getElementById("notodo_story").innerHTML=data.notodo_story;
          document.getElementById("noinpro_story").innerHTML=data.noinpro_story;
          document.getElementById("noinpro_defect").innerHTML=data.noinpro_defect;
          document.getElementById("nodone_story").innerHTML=data.nodone_story;
          document.getElementById("nodone_defect").innerHTML=data.nodone_defect;
          $('#HOMEbody').css({ 'opacity' : 1 });
          $("#loader").hide();
  
  });
});
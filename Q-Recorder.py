from selenium import webdriver
from selenium.webdriver.chrome.options import Options
driver=''
JS=open("smart_xpath.js").read()
def locate(xpath):

    global driver
    try:
        element=driver.find_element_by_xpath(xpath)
        driver.execute_script("arguments[0].style.border = '0.2em solid #0d274c';",element)
        return "PASS"
    except:
        return "FAIL"

def initiate_driver(url):
    global driver
    
    driver = webdriver.Chrome(executable_path ="chromedriver.exe")
    driver.get(url)
    
    
initiate_driver("https://google.com")
driver.execute_script(JS)


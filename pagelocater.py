# -*- coding: utf-8 -*-
"""
Created on Wed Jun 26 12:14:22 2019

@author: 666647
"""

import os
import pandas as pd
import re


def read_data():
    data = pd.read_excel('F:\\MyApp\\Q-MatePro Flask\\PAGEACTION\\JAVADatachanged.xls', index_col=None, header=None)
    return data


def get_the_function(function):
    results2=re.findall(r'\S+(?=\()',function )
    final_func=[]
    for i in results2:
        func_name=''
        func=0
        for j in i:
            if j =='.':
                func=1
            if func==1 and j!='.':
                func_name=func_name+j
        if func==1:
            #func_name=func_name
            final_func.append(func_name)
        #print(j)
    return final_func

    
    
def search(func_name):
    #keyword = input("openPage")  # ask the user for keyword, use raw_input() on Python 2.x
    file=[]
    file_path=''
    root_dir = "F:\\MyApp\\Q-MatePro Flask\\PAGEACTION\\tcsbitbucket-updatedframework-bcb45f949034\\tcs"  # path to the root directory to search
    for root, dirs, files in os.walk(root_dir, onerror=None): # walk the root dir
        #print(files)
        for filename in files:  # iterate over the files in the current dir
            file_path = os.path.join(root, filename)  # build the file path
            try:
                with open(file_path, "rb") as f:  # open the file for reading
                    # read the file line by line
                    for line in f:  # use: for i, line in enumerate(f) if you need line numbers
                        try:
                            line = line.decode("utf-8")  # try to decode the contents to utf-8
                        except ValueError:  # decoding failed, skip the line
                            continue
                        if func_name in line:  # if the keyword exists on the current line...
                            file.append(file_path)  # print the file path
                            break  # no need to iterate over the rest of the file
            except (IOError, OSError):  # ignore read and permission errors
                pass
            
    for i in file:
        if 'StepDef' in i:
            pass
        else:
            file_path=i
    return file_path


    
def function_definition(file_path,func_name):
    new_path=''
    '''if ('\\') in file_path:
        for i in file_path.split('\\'):
            new_path=new_path+i+'/'
        file_path=new_path'''
    with open(file_path) as f:
        content = f.readlines()
    # you may also want to remove whitespace characters like `\n` at the end of each line
    content = [x.strip() for x in content]
    open_braces=0
    close_braces=0
    name_found=0
    flag=0
    func_def=''
    for i in content:
        if func_name in i :
            if '{' in i:
                open_braces=open_braces+1
                flag=1
            func_def=func_def+i+'\n'
            name_found=1
            
          
        if name_found==1 and i!='}' and i!='{' and func_name not in i:
            func_def=func_def+i+'\n'

        
        if name_found==1 and flag!=1 and i=='{':
            open_braces=open_braces+1
            func_def=func_def+i+'\n'
            #print(func_def)
            
        elif open_braces==close_braces and open_braces>=1 and name_found==1:
            print('ookk')
            break
        
        elif name_found==1 and '}' in i:
            close_braces=close_braces+1
            func_def=func_def+i+'\n'
            #print(func_def)
    if func_def=='':
        func_def='\n\n\n\n\nInbuilt Method'
    print('\n\n\n',open_braces,close_braces)
    return func_def
    


def get_the_path(key_text,text):
    func_name=get_the_function(text)
    func_path=[]
    func_def=[]
    func_dict={}
    for i in func_name:
        func_path.append(search(i))
    for i in range(0,len(func_path)):
        func_def.append(function_definition(func_path[i],func_name[i]))
    
    for i in range(0,len(func_path)):
        if func_def[i]!='\n\n\n\n\nInbuilt Method':
            func_dict[func_name[i]+"6"+key_text]=func_def[i]
    return func_dict

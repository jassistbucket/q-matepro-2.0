import json
from flask import Flask, render_template,redirect,jsonify
from flask import request
import selenium
from flask import *  
import database_handeller
import os
import PL_PA
import ast
import requests
import socket
import Sprint_Tracker
import script_gen
import featurebot
import bitbot
import KER_CHAT
import dep_parse
app = Flask(__name__)
app.secret_key = "abc"

@app.route('/',methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form.get('username') # Your form's
        password = request.form.get('password')
        project_id=request.form.get('proj_select').split("&&")[1]
        project_key=request.form.get('proj_select').split("&&")[0]
        
        if username=="masteruser" and password=="password":
            session['user'] = username
            session['project_key'] = project_key
            session['project_id'] = project_id
            return redirect('/home')
        else:
            return redirect('/')
    else:
        try:
            Scrums=Sprint_Tracker.get_scrums()
            
            return render_template('login/login.html',Scrums=Scrums)
        except requests.exceptions.ConnectionError:
            return render_template('connectionerror/connectionerror.html')
@app.route('/home',methods=['GET', 'POST'])
def home():
    try:
        u=session['user']
        calender_defect,calender_story,calender_backlogs,nodefect,noback,nostory,notodo_defect,notodo_story,noinpro_story,noinpro_defect,nodone_story,nodone_defect=Sprint_Tracker.get_issuesByDate(session["project_key"],'ALL')
        Sprints=Sprint_Tracker.get_sprints(session["project_id"])
        return render_template('home/home.html',user=u,calender_defect=json.dumps(calender_defect),calender_story=json.dumps(calender_story),calender_backlogs=json.dumps(calender_backlogs),nodefect=nodefect,Sprints=Sprints,noback=noback,nostory=nostory,notodo_defect=notodo_defect,notodo_story=notodo_story,noinpro_defect=noinpro_defect,noinpro_story=noinpro_story,nodone_defect=nodone_defect,nodone_story=nodone_story)
    except KeyError as e:
        print(e)
        return redirect('/')
    except requests.exceptions.ConnectionError:
        return "Q-MatePro requires internet connection which you don't have at this moment"
@app.route('/get_by_sprint')
def get_by_sprint():
    sprint_id = request.args.get('sprint_id', 0, type=str)
    calender_defect,calender_story,calender_backlogs,nodefect,noback,nostory,notodo_defect,notodo_story,noinpro_story,noinpro_defect,nodone_story,nodone_defect=Sprint_Tracker.get_issuesByDate(session["project_key"],sprint_id)
    return jsonify(calender_defect=calender_defect,calender_story=calender_story,calender_backlogs=calender_backlogs,nodefect=nodefect,noback=noback,nostory=nostory,notodo_defect=notodo_defect,notodo_story=notodo_story,noinpro_defect=noinpro_defect,noinpro_story=noinpro_story,nodone_defect=nodone_defect,nodone_story=nodone_story)
@app.route('/getcart',methods=['GET', 'POST'])
def getcart():
    print("Getting Items>")
    cartitems=database_handeller.get_items()
    return jsonify(ITEMS=cartitems)
@app.route('/add_cart',methods=['GET', 'POST'])
def add_cart():
    PageAct_Text = request.args.get('PageAct_Text', 0, type=str)
    PageLoc_Text = request.args.get('PageLoc_Text', 0, type=str)
    filename_text=request.args.get('filename_text', 0, type=str)
    try:
        cartitems=database_handeller.add_items(PageAct_Text,PageLoc_Text,filename_text)
    
        return jsonify(status="SUCCESS")
    except Exception as e:
        print(str(e))
        return jsonify(status="FAIL")

@app.route('/add_cart_script',methods=['GET', 'POST'])
def add_cart_script():
    script = request.args.get('script', 0, type=str)
    
    filename_text=request.args.get('filename_text', 0, type=str)
    try:
        cartitems=database_handeller.add_items_script(script,filename_text)
    
        return jsonify(status="SUCCESS")
    except Exception as e:
        print(str(e))
        return jsonify(status="FAIL")

@app.route('/edit_cart',methods=['GET', 'POST'])
def edit_cart():
    ID = request.args.get('ID', 0, type=str)
    lang = request.args.get('chat', 0, type=str)
    nam=request.args.get('nam', 0, type=str)
    cartitems=database_handeller.edit_items(ID,nam,lang)
    
    return jsonify(ITEMS="SUCCESS")
@app.route('/del_cart',methods=['GET', 'POST'])
def del_cart():
    ID = request.args.get('ID', 0, type=str)

    cartitems=database_handeller.del_items(ID)
    return jsonify(ITEMS="SUCCESS")
@app.route('/collect')
def collect():
        url=request.args.get('chat', 0, type=str)
        try:
            L=PL_PA.main()
            if (L=="NOWINDOW"):
                return jsonify(result=L,status="NOWINDOW")
            return jsonify(result=L,status="PASS")
        except (selenium.common.exceptions.WebDriverException,AttributeError) as e:
            print(str(e))
            return jsonify(status="FAIL")

@app.route('/to_pageloc')
def to_pageloc():
    
    xpath=request.args.get('xpath', 0, type=str)
    name=request.args.get('name', 0, type=str)
    tag=request.args.get('tag', 0, type=str)
    xpath=ast.literal_eval(xpath)
    name=ast.literal_eval(name)
    tag=ast.literal_eval(tag)
    L=PL_PA.pageLocatorCreation(name,xpath)
    L2=PL_PA.pageActionCreation(tag,name,xpath)
    return jsonify(result=L,result2=L2)




@app.route('/to_browse')
def to_browse():
        url=request.args.get('chat', 0, type=str)
        s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        result=s.connect_ex(('127.0.0.1',9014))
        if result==0:
            s.close()
            return jsonify(status="FAIL")
        else:
            s.close()
            cmd = 'start chrome -remote-debugging-port=9014 --user-data-dir="D:\chrome"'
            os.popen(cmd)
            PL_PA.initiate_driver(url)
            print("Ok")
            return jsonify(status="PASS")
@app.route('/locate')
def locate():
    xpath=request.args.get('xpath', 0, type=str)
    status=PL_PA.locate(xpath)
    

    return jsonify(status=status)
@app.route('/home/recorder',methods=['GET', 'POST'])
def recorder():
    try:
        myurl=PL_PA.getCurrentUrl()
        return render_template('recorder/recorder.html',myurl=myurl)
    except AttributeError:
        return "Driver is disconnected. Launch the browser again"
@app.route('/QRec',methods=['GET', 'POST'])
def QRec():
    try:
        data=PL_PA.Q_recorder()
        return jsonify(data=data)
    except AttributeError:
        data="STOP"
        return jsonify(data=data)
    except selenium.common.exceptions.WebDriverException:
        data="STOP"
        return jsonify(data=data)
@app.route('/QRec_Stop',methods=['GET', 'POST'])
def QRec_Stop():
    PL_PA.stop()
    data="Stopped"
    return jsonify(data=data)


@app.route('/generate_script')
def generate_script():
    text=''
    print('generating ssss')  
    chat=request.args.get('chat', 0, type=str)
    nam=request.args.get('nam',0,type=str)
    print(chat)
    print(nam)
    chat=chat.lower()
    for line in chat.split('\n'):
        i=0
        for word in line.split():
            i=i+1
            if i==1:
                word=word.capitalize()
            text=text+word+' '
    script=script_gen.script_gen_main(text,nam)
    
    script=json.dumps(script,sort_keys=False)
    print('\n\n\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@script\n\n\n',script)
        
    return jsonify(result=str(script))
@app.route('/to_script')
def to_script():

    text=''
    print('toto ssss')  
    chat=request.args.get('chat', 0, type=str)
    nam=request.args.get('nam',0,type=str)
    given_func=request.args.get('given_func',0,type=str)
    when_func=request.args.get('when_func',0,type=str)
    then_func=request.args.get('then_func',0,type=str)
    print("GIVEN DIC",given_func)
    print("WHEN DIC",when_func)
    print("THEN DIC",then_func)
    import json
    given_func=json.loads(given_func)
    when_func=json.loads(when_func)
    then_func=json.loads(then_func)
    print("GIVEN DIC",type(given_func))
    print("WHEN DIC",type(when_func))
    print("THEN DIC",type(then_func))
    chat=chat.lower()
    for line in chat.split('\n'):
        i=0
        for word in line.split():
            i=i+1
            if i==1:
                word=word.capitalize()
            text=text+word+' '
    
    script,PA=script_gen.script_gen_main_final(text,nam,given_func,when_func,then_func)
    return jsonify(result=script,PA=PA)
@app.route('/featprocessing')
def featprocessing():
    
        message='';  
        m=''
        result=[]
        print('asasa')
        lang = request.args.get('chat', 0, type=str)
        print(lang)
        if str(lang.strip())=='scenario' or str(lang.strip())=='scenarios':
                print('ss',lang)
                m=random.choice(SCEN)
                d={'Repo':str(m),'File Name': str(m),'Path':str(m),'Code':str(m),'No_Result':'ROBO'}
                result.append(d)
                message=result
        else:
            print('langgg',lang)
            
            robo_response=KER_CHAT.KER_main(lang)
            
            #print(e)
            #robo_response='SORRY'
            print('robo res in app',robo_response)
            if robo_response=='SORRY':
                message=featurebot.get_results(lang)
                print('sasadsdaasdasas',message)
                if message=='scen':
                    m=random.choice(SCEN)
                    d={'Repo':str(m),'File Name': str(m),'Path':str(m),'Code':str(m),'No_Result':'ROBO'}
                    result.append(d)
                    message=result
                
                
            else:
                #message=robo_response
                if robo_response=='SPRINTSTAT' or robo_response=='ISSUETOT' or robo_response=='BACKNO' or robo_response=='DEFECTNO' or robo_response=='XPATH':
                        d={'Repo':str(robo_response),'File Name': str(robo_response),'Path':str(robo_response),'Code':str(robo_response),'No_Result':str(robo_response)}
                        result.append(d)
                        message=result
                        message=json.dumps(message)
                        return jsonify(resul=message,lang=lang)
                else:
                    d={'Repo':str(robo_response),'File Name': str(robo_response),'Path':str(robo_response),'Code':str(robo_response),'No_Result':str(robo_response)}
                    result.append(d)
                    message=result
                    message=json.dumps(message)
                    return jsonify(resul=message,lang=lang)

                        
                
        print('robo mes',message)
        message=json.dumps(message)
        print('got message')
        print(message)
        
        
        return jsonify(resul=message,lang=lang)
    
@app.route('/GOOG')
def GOOG():
    RES=[]
    
    
  
    # to search
    lang = request.args.get('chat', 0, type=str)
    query = lang
    r=robo.robo_main(query)
    if r=='SORRY':
        jq='This query belongs to a restricted category as per Q-MatePro policy'
        d={'URL':'None','TITLE':jq}
        RES.append(d)
    else:
        try:
            for j in search(query, tld="co.in", num=10, stop=5, pause=2): 
     
                d={'URL':str(j),'TITLE':str(j)}
                RES.append(d)
        except Exception as e:
            print('ERRROr\n\n',e)
            RES=[]
            jq="Sorry! Could'nt connect to Google.Try again!"
            d={'URL':'None','TITLE':jq}
            RES.append(d)
    
        


    return jsonify(result=RES)
@app.route('/bitprocessing')
def bitprocessing():
    
        message=''
        print('asasa')
        lang = request.args.get('chat', 0, type=str)
        print('ss',lang)
        
        
        COUNT_SPACE=lang.count(' ')
        if COUNT_SPACE<2:
            lang_1=lang[0:4]
            message=bitbot.get_results2(lang_1,lang)
            message=json.dumps(message)
            
        else:
            
            d1=dep_parse.main_process(lang)
            
            qu=d1['Beta']
            if len(qu)>1:
                result=[]
                print('more len')
                d={'Repo':"No Match Found",'File Name': "No Match Found",'Path':"No Match Found",'Function':"No Match Found",'Line No':"No Match Found"}
                result.append(d)
                message=result
                message=json.dumps(message)
            else:
                lang_sp=qu[0]
                lang_1sp=lang_sp[0:4]
                message=bitbot.get_results2(lang_1sp,lang_sp)
                message=json.dumps(message)
                
                
            
        
        
        print('got message')
        print(message)
        
        
        return jsonify(resul=message,lang=lang)
    
if __name__ == '__main__':
   app.run(debug = True)